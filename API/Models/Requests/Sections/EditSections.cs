using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication3.API.Models.Requests.Sections
{
    public class EditSections
    {
        [Required]
        public List<string> SectionsNames { get; set; }
    }
}