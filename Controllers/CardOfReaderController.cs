using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication3.API.Models.Requests.CardOfReader;
using WebApplication3.Entity.Entities;
using WebApplication3.MvcExt;
using WebApplication3.Services;

namespace WebApplication3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardOfReaderController : ControllerBase
    {
        private readonly ICardOfReaderService _cardOfReaderSvc;

        public CardOfReaderController(ICardOfReaderService cardOfReaderSvc)
        {
            _cardOfReaderSvc = cardOfReaderSvc;
        }
        
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IList<CardOfReader>>> Get()
        {
            return new ActionResult<IList<CardOfReader>>(await _cardOfReaderSvc.GetAll());
        }
        
        
        // GET api/messages/?userId=2
        [HttpGet]
        [ExactQueryParam("readerId")]
        public async Task<ActionResult<IEnumerable<CardOfReader>>> GetByReaderId(int readerId)
        {
            return new ActionResult<IEnumerable<CardOfReader>>(await _cardOfReaderSvc.GetByReaderId(readerId));
        }
        
        [HttpGet]
        [ExactQueryParam("bookId")]
        public async Task<ActionResult<IEnumerable<CardOfReader>>> GetByBookId(int bookId)
        {
            return new ActionResult<IEnumerable<CardOfReader>>(await _cardOfReaderSvc.GetByBookId(bookId));
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CardOfReader>> Get(int id)
        {
            var message = await _cardOfReaderSvc.GetById(id);
            if (message == null)
            {
                return new NotFoundObjectResult(null);
            }
            return (message);
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult<CardOfReader>> Post([FromBody] CreateCardOfReader value)
        {
            return await _cardOfReaderSvc.Create(value);
        }

        // PUT api/cardOfReader/5
        [HttpPut("{id}")]
        public async Task<ActionResult<CardOfReader>> Put(int id, [FromBody] EditCardOfReader value)
        {
            await _cardOfReaderSvc.Update(id, value.Give_Time);
            return await _cardOfReaderSvc.GetById(id);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _cardOfReaderSvc.Delete(id);
        }
    }
}