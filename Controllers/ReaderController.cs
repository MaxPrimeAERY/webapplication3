using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication3.API.Models.Requests.Reader;
using WebApplication3.Entity.Entities;
using WebApplication3.MvcExt;
using WebApplication3.Services;

namespace WebApplication3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReaderController : ControllerBase
    {
       private readonly IReaderService _readerSvc;

        public ReaderController(IReaderService readerSvc)
        {
            _readerSvc = readerSvc;
        }
        
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IList<Reader>>> Get()
        {
            return new ActionResult<IList<Reader>>(await _readerSvc.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Reader>> Get(int id)
        {
            var reader = await _readerSvc.GetById(id);
            if (reader == null)
            {
                return new NotFoundObjectResult(null);
            }
            return reader;
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult<Reader>> Post([FromBody] CreateReader value)
        {
            return await _readerSvc.Create(value);
        }

        // PUT api/reader/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Reader>> Put(int id, [FromBody] EditReader value)
        {
            
            await _readerSvc.Update(id, value.Name);
            return await _readerSvc.GetById(id);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _readerSvc.Delete(id);
        }
    }
}