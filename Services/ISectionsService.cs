using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication3.API.Models.Requests.Sections;
using WebApplication3.Entity.Entities;

namespace WebApplication3.Services
{
    public interface ISectionsService
    {
        Task<Sections> GetById(int id);
        Task<IList<Sections>> GetAll();
        Task Delete(int id);
        Task Update(int sectionsId, List<string> valueLists);
        Task<Sections> Create(CreateSections value);
    }
}