using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication3.API.Models.Requests.Author;
using WebApplication3.DAL.Abstract;
using WebApplication3.Entity.Entities;
using WebApplication3.Services;

namespace WebApplication3.Services.Impl
{
    public class AuthorService : IAuthorService
    {
        private IAuthorRepository _authRepo;

        public AuthorService(IAuthorRepository authRepo)
        {
            _authRepo = authRepo;

        }
        public Task<Author> GetById(int id)
        {
            return _authRepo.GetById(id);
        }

        public Task<IList<Author>> GetAll()
        {
            return _authRepo.GetAll();
        }

        public async Task Delete(int id)
        {
            var res = await _authRepo.Delete(id);
            if (!res)
            {
                throw new Exception("Not found.");
            }
        }
        
        public  async Task<Author> Create(CreateAuthor value)
        {
            var Author = new Author()
            {
                Name = value.Name
            };

            var id = await _authRepo.Insert(Author);
            Author.Id = id;
            return Author;
        }

        public async Task Update(int authId, string valueName)
        {
            var Author = await _authRepo.GetById(authId);
            Author.Name = valueName;
            await _authRepo.Update(Author);
        }

       
    }
}