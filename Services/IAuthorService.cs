using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication3.API.Models.Requests.Author;
using WebApplication3.API.Models.Requests.Books;
using WebApplication3.Entity.Entities;

namespace WebApplication3.Services
{
    public interface IAuthorService
    {
        Task<Author> GetById(int id);
        Task<IList<Author>> GetAll();
        Task Delete(int id);
        Task Update(int authorId, string valueText);
        Task<Author> Create(CreateAuthor value);
    }
}