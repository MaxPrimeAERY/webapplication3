using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication3.API.Models.Requests.Reader;
using WebApplication3.Entity.Entities;

namespace WebApplication3.Services
{
    public interface IReaderService
    {
        Task<Reader> GetById(int id);
        Task<IList<Reader>> GetAll();
        Task Delete(int id);
        Task Update(int readerId, string valueText);
        Task<Reader> Create(CreateReader value);
    }
}