using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebApplication3.Entity.Entities.Abstract;

namespace WebApplication3.Entity.Entities
{
    public class BookOfAuthor:IBaseEntity<int>
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [ForeignKey("Author")]
        [Column("author_id")]
        public int? AuthorId { get; set; }
        [ForeignKey("Books")]
        [Column("book_id")]
        public int? BookId { get; set; }
        
    }
}