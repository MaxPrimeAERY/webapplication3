namespace WebApplication3.Entity.Entities.Abstract
{
    public interface IBaseEntity<TKey>
    {
        TKey Id { get; }
    }
}