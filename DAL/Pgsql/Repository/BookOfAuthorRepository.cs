using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WebApplication3.DAL.Abstract;
using WebApplication3.DAL.EntityFramework.Repository;
using WebApplication3.DAL.Pgsql.Infrastructure;
using WebApplication3.Entity.Entities;
using AppContext = WebApplication3.DAL.EntityFramework.AppContext;

namespace WebApplication3.DAL.Pgsql.Repository
{
    internal class BookOfAuthorRepository  : GlobalRepository<int, BookOfAuthor>, IBookOfAuthorRepository
    {
        public BookOfAuthorRepository(AppContext ctx) : base(ctx)
        {
            
        }
        
        public async Task<IList<BookOfAuthor>> GetByAuthorId(int authorId)
        {
            return await Context.Set<BookOfAuthor>()
                .Where(p => p.AuthorId == authorId)
                .ToListAsync();
        }

        public async Task<IList<BookOfAuthor>> GetByBookId(int bookId)
        {
            return await Context.Set<BookOfAuthor>()
                .Where(p => p.BookId == bookId)
                .ToListAsync();
        }
        
//        public async Task<IList<BookOfAuthor>> GetAll()
//        {
//            return await Context.Set<BookOfAuthor>().ToListAsync();
//        }
//        
//        public async Task<int> Insert(BookOfAuthor entity)
//        {
//            var item = await Context.Set<BookOfAuthor>().AddAsync(entity);
//            await SaveAsync();
//            return item.Entity.Id;
//        }
//
//        public async Task<bool> Update(BookOfAuthor entity)
//        {
//            Context.Entry(entity).State = EntityState.Modified;
//            await SaveAsync();
//            return true;
//        }
//
//        public async Task<int> Upsert(BookOfAuthor entity)
//        {
//            if (Object.Equals(entity.Id, default(int)))
//                return await Insert(entity);
//            else
//            {
//                if (await Update(entity))
//                    return entity.Id;
//                else
//                    return default(int);
//            }
//        }
//
//        public async Task<int> GetCount()
//        {
//            return await Context.Set<BookOfAuthor>().CountAsync();
//        }
//
//        public async Task<BookOfAuthor> GetById(int id)
//        {
//            return await Context.Set<BookOfAuthor>()
//                .FindAsync(id);
//        }
//
//        public async Task<bool> Delete(int id)
//        {
//            BookOfAuthor element =
//                await GetById(id);
//            BookOfAuthor result = Context.Set<BookOfAuthor>()
//                .Remove(element).Entity;
//            return 0 != await SaveAsync();
//        }

    }
}