using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication3.Entity.Entities;

namespace WebApplication3.DAL.Abstract
{
    public interface IBookOfAuthorRepository : IBaseRepository<int, BookOfAuthor>
    {
        Task<IList<BookOfAuthor>> GetByAuthorId(int fullId);
        
        Task<IList<BookOfAuthor>> GetByBookId(int fullId);
    }
}