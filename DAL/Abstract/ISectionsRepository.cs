using WebApplication3.Entity.Entities;

namespace WebApplication3.DAL.Abstract
{
    public interface ISectionsRepository : IBaseRepository<int, Sections>
    {
        
    }
}