using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication3.Entity.Entities;

namespace WebApplication3.DAL.Abstract
{
    public interface IBooksRepository : IBaseRepository<int, Books>
    {
        Task<IList<Books>> GetBySectionsId(int sectId);
    }
}