using System.Collections.Generic;
using System.Threading.Tasks;
using WebApplication3.Entity.Entities;
using WebApplication3.Entity.Entities.Abstract;

namespace WebApplication3.DAL.Abstract
{
    public interface IBaseRepository<TKey, TEntity> where TEntity : IBaseEntity<TKey>
    {
        Task<TKey> Insert(TEntity entity);
        Task<bool> Update(TEntity entity);
        Task<TKey> Upsert(TEntity entity);

        Task<int> GetCount();

        Task<TEntity> GetById(TKey id);
        Task<bool> Delete(TKey id);

        Task<IList<TEntity>> GetAll();
    }
}